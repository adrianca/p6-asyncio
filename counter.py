import asyncio

async def counter(id, time):
    print(f'Counter {id}: starting...')
    await asyncio.sleep(0)#aqui le mando dormir pero con un tiempo de 0 segundos, consiguiendo que los contadores se inicien a la vez

    for i in range(1, time + 1):
        print(f'Contador {id}: {i}')
        if i == time:
            break
        await asyncio.sleep(1)#espera 1 seg antes de empezar el siguiente ciclo

    print(f'Counter {id}: finishing...')

async def main():
    await asyncio.gather(
        counter('A', 4),
        counter('B', 2),
        counter('C', 6))


asyncio.run(main())
