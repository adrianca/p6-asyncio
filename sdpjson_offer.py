import asyncio
import sdp_transform
import json

sdp_offer = {
    'version': 0,
    'origin': {'username': 'user',
               'sessionId': 434344,
               'sessionVersion': 0,
               'netType': 'IN',
               'ipVer': 4,
               'address': '127.0.0.1'},
    'name': 'Session',
    'timing': {'start': 0, 'stop': 0},
    'connection': {'version': 4, 'ip': '127.0.0.1'},
    'media': [{'rtp': [{'payload': 0, 'codec': 'PCMU', 'rate': 8000},
                       {'payload': 96, 'codec': 'opus', 'rate': 48000}],
               'type': 'audio',
               'port': 54400,
               'protocol': 'RTP/SAVPF',
               'payloads': '0 96',
               'ptime': 20,
               'direction': 'sendrecv'},
              {'rtp': [{'codec': 'H264', 'payload': 97, 'rate': 90000},
                       {'codec': 'VP8', 'payload': 98, 'rate': 90000}],
               'type': 'video',
               'port': 55400,
               'protocol': 'RTP/SAVPF',
               'payloads': '97 98',
               'direction': 'sendrecv'}]
}


# Existing imports

class EchoClientProtocol:
    def __init__(self, sdp):
        self.sdp = sdp
        self.on_con_lost = asyncio.Future()
        self.transport = None
    def connection_made(self, transport):
        self.transport = transport

        offer_json = {
            "type": "offer",
            "sdp": sdp_transform.write(sdp_offer)
        }

        self.transport.sendto(json.dumps(offer_json).encode())

    def datagram_received(self, data, addr):
        response = json.loads(data.decode())
        sdp = sdp_transform.parse(response['sdp'])
        print("Received:", sdp_transform.write(sdp))

        self.transport.close()

    # Rest of main() unchanged

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)
async def main():
    loop = asyncio.get_event_loop()
    sdp = sdp_transform.write(sdp_offer)
    on_con_lost = loop.create_future()

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoClientProtocol(sdp),
        remote_addr=('127.0.0.1', 9998))

    try:
        await on_con_lost
    finally:
        transport.close()


if __name__ == "__main__":
    asyncio.run(main())
