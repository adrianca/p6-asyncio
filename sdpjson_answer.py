import asyncio
import sdp_transform
import json


class EchoServerProtocol:

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        offer_json = json.loads(data.decode())
        offer = sdp_transform.parse(offer_json['sdp'])

        print('Received JSON offer:')
        print(offer)

        answer = generate_sdp_answer(offer)

        print('Sending JSON answer:')
        print(answer)

        response = {
            "type": "answer",
            "sdp": sdp_transform.write(answer)
        }

        self.transport.sendto(json.dumps(response).encode(), addr)


def generate_sdp_answer(offer):
    answer = offer.copy()

    answer['media'][0]['port'] = 34543
    answer['media'][1]['port'] = 34543

    return answer


async def main():
    print('Starting server')

    loop = asyncio.get_running_loop()

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 9998))

    try:
        await asyncio.sleep(3600)
    finally:
        transport.close()


asyncio.run(main())
